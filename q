[1mdiff --git a/app/src/main/java/com/example/fafa/meau/CadastrarAnimalApadrinhar.java b/app/src/main/java/com/example/fafa/meau/CadastrarAnimalApadrinhar.java[m
[1mindex 3185e0b..063f210 100644[m
[1m--- a/app/src/main/java/com/example/fafa/meau/CadastrarAnimalApadrinhar.java[m
[1m+++ b/app/src/main/java/com/example/fafa/meau/CadastrarAnimalApadrinhar.java[m
[36m@@ -69,7 +69,7 @@[m [mpublic class CadastrarAnimalApadrinhar extends Fragment {[m
                              Bundle savedInstanceState) {[m
         // Inflate the layout for this fragment[m
         View view = inflater.inflate(R.layout.fragment_cadastrar_animal_apadrinhar, container, false);[m
[31m-        inputPhoto = view.findViewById(R.id.inputPhoto);[m
[32m+[m[32m        inputPhoto = view.findViewById(R.id.inputPhotoApadrinhar);[m
         return view;[m
     }[m
 [m
[36m@@ -122,8 +122,8 @@[m [mpublic class CadastrarAnimalApadrinhar extends Fragment {[m
      * "http://developer.android.com/training/basics/fragments/communicating.html"[m
      * >Communicating with Other Fragments</a> for more information.[m
      */[m
[31m-    public interface OnFragmentInteractionListener {[m
[31m-        // TODO: Update argument type and name[m
[31m-        void onFragmentInteraction(Uri uri);[m
[31m-    }[m
[32m+[m[32m//    public interface OnFragmentInteractionListener {[m
[32m+[m[32m//        // TODO: Update argument type and name[m
[32m+[m[32m//        void onFragmentInteraction(Uri uri);[m
[32m+[m[32m//    }[m
 }[m
[1mdiff --git a/app/src/main/java/com/example/fafa/meau/cadastroAnimal.java b/app/src/main/java/com/example/fafa/meau/cadastroAnimal.java[m
[1mindex 0e3559b..10e81dc 100644[m
[1m--- a/app/src/main/java/com/example/fafa/meau/cadastroAnimal.java[m
[1m+++ b/app/src/main/java/com/example/fafa/meau/cadastroAnimal.java[m
[36m@@ -12,22 +12,22 @@[m [mimport android.widget.Toast;[m
 public class cadastroAnimal extends AppCompatActivity {[m
 [m
     private Fragment actionFragment;[m
[31m-    private Button adotarButton, apadrinharButton, ajudarButton;[m
[32m+[m[32m    private Button buttonAdocao, buttonApadrinhar, buttonAjudar;[m
 [m
     @Override[m
     protected void onCreate(Bundle savedInstanceState) {[m
         super.onCreate(savedInstanceState);[m
         setContentView(R.layout.activity_cadastro_animal);[m
         if(savedInstanceState != null) return;[m
[31m-        adotarButton = (Button) findViewById(R.id.buttonAdocao);[m
[31m-        apadrinharButton = (Button) findViewById(R.id.buttonApadrinhar);[m
[31m-        ajudarButton = (Button) findViewById(R.id.buttonApadrinhar);[m
[32m+[m[32m        buttonAdocao = (Button) findViewById(R.id.buttonAdocao);[m
[32m+[m[32m        buttonApadrinhar = (Button) findViewById(R.id.buttonApadrinhar);[m
[32m+[m[32m        buttonAjudar = (Button) findViewById(R.id.buttonAjudar);[m
     }[m
 [m
     @Override[m
     protected void onStart() {[m
         super.onStart();[m
[31m-        adotarButton.setOnClickListener(new View.OnClickListener() {[m
[32m+[m[32m        buttonAdocao.setOnClickListener(new View.OnClickListener() {[m
             @Override[m
             public void onClick(View v) {[m
                 actionFragment = new CadastrarAnimalAdocao();[m
[36m@@ -35,7 +35,7 @@[m [mpublic class cadastroAnimal extends AppCompatActivity {[m
                 ft.replace(R.id.activityCadastroAnimalFragment, actionFragment).commit();[m
             }[m
         });[m
[31m-        apadrinharButton.setOnClickListener(new View.OnClickListener() {[m
[32m+[m[32m        buttonApadrinhar.setOnClickListener(new View.OnClickListener() {[m
             @Override[m
             public void onClick(View v) {[m
                 actionFragment = new CadastrarAnimalApadrinhar();[m
[36m@@ -43,7 +43,7 @@[m [mpublic class cadastroAnimal extends AppCompatActivity {[m
                 ft.replace(R.id.activityCadastroAnimalFragment, actionFragment).commit();[m
             }[m
         });[m
[31m-        ajudarButton.setOnClickListener(new View.OnClickListener() {[m
[32m+[m[32m        buttonAjudar.setOnClickListener(new View.OnClickListener() {[m
             @Override[m
             public void onClick(View v) {[m
                 showToast("Not yet!");[m
[36m@@ -55,9 +55,9 @@[m [mpublic class cadastroAnimal extends AppCompatActivity {[m
     protected void onDestroy() {[m
         super.onDestroy();[m
         actionFragment = null;[m
[31m-        adotarButton = null;[m
[31m-        apadrinharButton = null;[m
[31m-        ajudarButton = null;[m
[32m+[m[32m        buttonAdocao = null;[m
[32m+[m[32m        buttonApadrinhar = null;[m
[32m+[m[32m        buttonAjudar = null;[m
     }[m
 [m
     private void showToast(String text){[m
