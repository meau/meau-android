package com.example.fafa.meau;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by marcella on 11/05/18.
 */

public class IntroActivity extends AppCompatActivity {

    private Button botaoAdotar;
    private Button botaoAjudar;
    private Button botaoPerfilAnimal;
    private Button botaoLogin;
    private static final String activity_intro = "activity_intro";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        Log.i(activity_intro, "onCreate");
        botaoAdotar = (Button) findViewById(R.id.buttonMenuAdotarID);
        botaoAjudar = (Button) findViewById(R.id.buttonMenuAjudarID);
        botaoPerfilAnimal = (Button) findViewById(R.id.buttonMenuCadastrar);
        botaoLogin = (Button) findViewById(R.id.buttonMenuLogin);
    }



    @Override
    protected void onStart(){
        super.onStart();
        Log.i(activity_intro, "onStart");
        if(botaoAdotar != null){
            botaoAdotar.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mAuth = FirebaseAuth.getInstance();
                    Log.i(activity_intro, "Before callings.");
                    if(mAuth.getCurrentUser() != null){
                        Log.i(activity_intro, "Calling adotar activity from intro activity.");
                        Intent intent = new Intent(IntroActivity.this, Adotar.class);
                        startActivity(intent);
//                        introToast("Adotar activity");
                    }
                    else {
                        Intent intent = new Intent(IntroActivity.this, OpsScreenActivity.class);
                        startActivity(intent);
                        Log.i(activity_intro, "Calling ops screen activity from intro activity.");
                        introToast("Você não está logado!");
                    }



                }
            });
        }
        else{
            introToast("erro botao adotar");
        }
        if(botaoAjudar != null){
            botaoAjudar.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mAuth = FirebaseAuth.getInstance();
                    Log.i(activity_intro, "Before callings.");
                    if(mAuth.getCurrentUser() != null){
//                        introToast("Ajudar activity not yet implemented");
                        Log.i(activity_intro, "Calling toast of ajudar activity from intro activity.");
//                        introToast("Ajudar activity");
                        Intent intent = new Intent(IntroActivity.this, AjudarActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(IntroActivity.this, OpsScreenActivity.class);
                        startActivity(intent);
                        Log.i(activity_intro, "Calling ops screen activity from intro activity.");
                        introToast("Você não está logado!");
                    }
                }

            });
        }
        else{
            introToast("erro botao ajudar");
        }

        if(botaoPerfilAnimal != null){
            botaoPerfilAnimal.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mAuth = FirebaseAuth.getInstance();
                    Log.i(activity_intro, "Before callings.");
                    if(mAuth.getCurrentUser() != null){
                        Intent intent = new Intent(IntroActivity.this, cadastroAnimal.class);
                        startActivity(intent);
                        introToast("Cadastrar animal.");
                        Log.i(activity_intro, "Calling cadastroAnimal activity from intro activity.");
//                        introToast("Cadastro animal activity");
                    }
                    else {
                        Intent intent = new Intent(IntroActivity.this, OpsScreenActivity.class);
                        startActivity(intent);
                        Log.i(activity_intro, "Calling ops screen activity from intro activity.");
                        introToast("Você não está logado!");
                    }

                }
            });
        }
        else{
            introToast("erro botao perfil animal");
        }
        if(botaoLogin != null){
            botaoLogin.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(IntroActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
        }
        else{
            introToast("erro botao login");
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.i(activity_intro, "onResume");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.i(activity_intro, "onPause");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.i(activity_intro, "onStop");
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.i(activity_intro, "onDestroy");
        botaoAdotar = (Button) null;
        botaoAjudar = (Button) null;
        botaoPerfilAnimal = (Button) null;
        botaoLogin = (Button) null;
    }

    private void introToast(String text){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }
}
