package com.example.fafa.meau;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.Iterator;
import java.util.Vector;

public class Privacidade extends AppCompatActivity {

    private Context contexto; //necessario pra gaveta
    Drawer result; //necessario pra gaveta



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacidade);



//INICIO DO CODIGO DA GAVETA
        new DrawerBuilder().withActivity(this).build();

        Gaveta gaveta = new Gaveta();

        contexto = this;


        //create the drawer and remember the `Drawer` result object
        result = new DrawerBuilder()
                .withActivity(this)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {


                        Intent intent = gaveta.getIntentById(contexto, drawerItem.getIdentifier());
                        if (intent != null) {
                            startActivity(intent);
                            result.closeDrawer();
                            return true;
                        }


                        return false;
                    }
                })
                .build();

        //percorre vetor de items e povoa o menu
        Vector itemsGaveta = gaveta.getLista();
        Iterator it = itemsGaveta.iterator();
        while (it.hasNext()) {
            result.addItem((IDrawerItem) it.next());
        }

        //FIM DO CODIGO DA GAVETA

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
