package com.example.fafa.meau;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class OpsScreenActivity extends AppCompatActivity {

    private Button botaoCadastro;
    private Button botaoLogin;
    private static final String activity_ops_screen = "activity_ops_screen";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ops_screen);
        Log.i(activity_ops_screen, "onCreate");
        botaoCadastro = findViewById(R.id.buttonOpsCadastro);
        botaoLogin = findViewById(R.id.buttonOpsLogin);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(activity_ops_screen, "onStart");
        if (botaoCadastro != null) {
            botaoCadastro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(activity_ops_screen, "Calling cadastro pessoal activity from ops screen activity.");
                    Intent intent = new Intent(OpsScreenActivity.this, CadastroPessoalActivity.class);
                    startActivity(intent);
//                    introToast("Cadastro pessoal activity not yet implemented");
                }
            });
        } else {
//             introToast("erro botao cadastrar");                                                 }
        }

        if (botaoLogin != null) {
            botaoLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(activity_ops_screen, "Calling login activity from ops screen activity.");
                    Intent intent = new Intent(OpsScreenActivity.this, MainActivity.class);
                    startActivity(intent);
//                    introToast("Login activity not yet implemented");
                }
            });
        } else {
//             introToast("erro botao login");                                                 }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        botaoCadastro = null;
        botaoLogin = null;
    }
}