package com.example.fafa.meau;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class AnimalAdapter extends RecyclerView.Adapter<AnimalAdapter.ViewHolder>{

    private List<Animal> animalDataSet;
    private Context context;

    public AnimalAdapter(List<Animal> animalList, Context context){
        this.animalDataSet = animalList;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final TextView gender;
        private final TextView age;
        private final TextView size;
        private final TextView address;
        private final ImageView image;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.animalName);
            gender = (TextView) view.findViewById(R.id.animalGender);
            age = (TextView) view.findViewById(R.id.animalAge);
            size = (TextView) view.findViewById(R.id.animalSize);
            address = (TextView) view.findViewById(R.id.animalAddress);
            image = (ImageView) view.findViewById(R.id.animalImage);
        }


        public void setName(String name) { this.name.setText(name); }
        public void setGender(String gender) { this.gender.setText(gender); }
        public void setAge(String age) { this.age.setText(age); }
        public void setSize(String size) { this.size.setText(size); }
        public void setAddress(String address) { this.address.setText(address); }
        public void setImage(int img) { Picasso.get().load(img).resize(0, 183).into(image); }
    }

    @NonNull
    @Override
    public AnimalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_animal, parent, false);
        AnimalAdapter.ViewHolder vh = new AnimalAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AnimalAdapter.ViewHolder vh = (AnimalAdapter.ViewHolder) holder;
        Animal animal = animalDataSet.get(position);

        holder.setName(animal.getName());
        holder.setGender(animal.getGender());
        holder.setAge(animal.getAge());
        holder.setSize(animal.getSize());
        holder.setAddress(animal.getAddress());
        holder.setImage(animal.getImage());
    }

    @Override
    public int getItemCount() {
        if(this.animalDataSet != null){
            return this.animalDataSet.size();
        }
        return 0;
    }
}
