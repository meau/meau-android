package com.example.fafa.meau;

public class Animal {

    private final String name;
    private final String gender;
    private final String age;
    private final String size;
    private final String address;
    private final int image;

    public Animal(String name, String gender, String age, String size, String address, int image){
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.size = size;
        this.address = address;
        this.image = image;
    }


    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getAge() {
        return age;
    }

    public String getSize() {
        return size;
    }

    public String getAddress() {
        return address;
    }

    public int getImage(){
        return image;
    }
}
