package com.example.fafa.meau;

import android.content.Context;
import android.content.Intent;

import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;

import java.util.Vector;

public class Gaveta {

    private Vector lista;



    public Gaveta() {

        lista = new Vector();


        lista.add(new PrimaryDrawerItem().withIcon(R.drawable.meau_marca));
        lista.add(new DividerDrawerItem());
        lista.add(new SecondaryDrawerItem().withIdentifier(1).withName("Menu Principal"));
        lista.add(new SecondaryDrawerItem().withIdentifier(6).withName("Adotar"));
        lista.add(new SecondaryDrawerItem().withIdentifier(2).withName("Cadastrar Animal"));
        lista.add(new SecondaryDrawerItem().withIdentifier(3).withName("Legislação"));
        lista.add(new SecondaryDrawerItem().withIdentifier(4).withName("Privacidade"));
        lista.add(new SecondaryDrawerItem().withIdentifier(5).withName("Sair"));

    }

    public Vector getLista() {
        return lista;
    }

    public Intent getIntentById(Context context, long id) {


        if(id == 1) {
            return new Intent(context, IntroActivity.class);
        }
        if(id == 2) {
            return new Intent(context, cadastroAnimal.class);
        }
        if(id == 3) {
            return new Intent(context, Legislacao.class);
        }
        if(id == 4) {
            return new Intent(context, Privacidade.class);
        }
        if(id == 5) {
            System.exit(0);
        }
        if(id == 6) {
            return new Intent(context, Adotar.class);
        }

        return null;
    }


}
