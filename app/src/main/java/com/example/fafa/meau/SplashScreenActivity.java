package com.example.fafa.meau;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by marcella on 11/05/18.
 */

public class SplashScreenActivity extends AppCompatActivity {
    private int SLEEP_TIMER = 3;
    private View contentView;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        contentView = findViewById(R.id.splashScreenId);
        actionBar = getSupportActionBar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        hide();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), IntroActivity.class));
                finish();
            }
        }, 1000 * SLEEP_TIMER);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        contentView = null;
        actionBar = null;
    }

    private void hide(){
        contentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        if(actionBar != null){
            actionBar.hide();
        }
    }
}
