package com.example.fafa.meau;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

public class Adotar extends AppCompatActivity {

    private RecyclerView rv;
    private ArrayList<Animal> animalList;

    private Context contexto; //necessario pra gaveta
    Drawer result; //necessario pra gaveta

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adotar);
        rv = findViewById(R.id.adotarRecyclerView);

        //INICIO DO CODIGO DA GAVETA
        new DrawerBuilder().withActivity(this).build();

        Gaveta gaveta = new Gaveta();

        contexto = this;


        //create the drawer and remember the `Drawer` result object
        result = new DrawerBuilder()
                .withActivity(this)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {


                        Intent intent = gaveta.getIntentById(contexto, drawerItem.getIdentifier());
                        if (intent != null) {
                            startActivity(intent);
                            result.closeDrawer();
                            return true;
                        }


                        return false;
                    }
                })
                .build();

        //percorre vetor de items e povoa o menu
        Vector itemsGaveta = gaveta.getLista();
        Iterator it = itemsGaveta.iterator();
        while(it.hasNext()) {
            result.addItem((IDrawerItem) it.next());
        }

        //FIM DO CODIGO DA GAVETA
    }

    @Override
    protected void onStart() {
        super.onStart();
        initAnimalList();
        rv.setAdapter(new AnimalAdapter(animalList, this));
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        rv = null;
        animalList = null;
    }

    private void initAnimalList(){ // get data from database
        animalList = new ArrayList<Animal>();
        animalList.add(new Animal("THOR", "MACHO", "ADULTO", "GRANDE", "ÁGUAS CLARAS - DISTRITO FEDERAL", R.drawable.dog8));
        animalList.add(new Animal("LILI", "FÊMEA", "ADULTO", "PEQUENO", "SUDOESTE - DISTRITO FEDERAL", R.drawable.dog7));
        animalList.add(new Animal("CINDY", "FÊMEA", "FILHOTE", "PEQUENO", "ÁGUAS CLARAS - DISTRITO FEDERAL", R.drawable.dog6));
        animalList.add(new Animal("PIERRE", "MACHO", "FILHOTE", "PEQUENO", "ASA SUL - DISTRITO FEDERAL", R.drawable.dog5));
        animalList.add(new Animal("SALSICHÃO", "MACHO","ADULTO", "PEQUENO", "ASA NORTE - DISTRITO FEDERAL", R.drawable.dog4));
        animalList.add(new Animal("PANDORA", "FÊMEA","ADULTO", "GRANDE", "ASA SUL - DISTRITO FEDERAL", R.drawable.dog3));
        animalList.add(new Animal("LUNA", "FÊMEA","ADULTO", "GRANDE", "ASA NORTE - DISTRITO FEDERAL", R.drawable.dog2));
        animalList.add(new Animal("KALI", "FÊMEA","ADULTO", "GRANDE", "ÁGUAS CLARAS - DISTRITO FEDERAL", R.drawable.dog1));
        animalList.add(new Animal("SHIBA", "MACHO","ADULTO", "GRANDE", "GUARÁ - DISTRITO FEDERAL", R.drawable.cat8));
        animalList.add(new Animal("ZEUS", "MACHO","ADULTO", "GRANDE", "ÁGUAS CLARAS - DISTRITO FEDERAL", R.drawable.cat7));
        animalList.add(new Animal("HITECH", "MACHO","ADULTO", "GRANDE", "JARDIM BOTÂNICO - DISTRITO FEDERAL", R.drawable.cat6));
        animalList.add(new Animal("TOKYO", "MACHO","FILHOTE", "PEQUENO", "ASA NORTE - DISTRITO FEDERAL", R.drawable.cat5));
        animalList.add(new Animal("GAIA", "FÊMEA","FILHOTE", "GRANDE", "ASA NORTE - DISTRITO FEDERAL", R.drawable.cat4));
        animalList.add(new Animal("OZZY", "MACHO","ADULTO", "GRANDE", "SUDOESTE - DISTRITO FEDERAL", R.drawable.cat3));
        animalList.add(new Animal("CHARLIE", "FÊMEA","FILHOTE", "PEQUENO", "ASA NORTE - DISTRITO FEDERAL", R.drawable.cat2));
        animalList.add(new Animal("PIZZA", "MACHO","ADULTO", "MÉDIO", "GUARÁ - DISTRITO FEDERAL", R.drawable.cat1));
    }
}
