package com.example.fafa.meau;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText emailText;
    private TextInputEditText passwordText;
    private Button buttonLogin;
    private Button buttonSignUp;
    private Button buttonSignOff;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogin = findViewById(R.id.ButtonLogin);
        buttonSignUp = findViewById(R.id.ButtonSignUp);
        emailText = findViewById(R.id.TextUserName);
        passwordText = findViewById(R.id.TextPassword);
        buttonSignOff = findViewById(R.id.ButtonLogOff);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth = FirebaseAuth.getInstance();
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailText.getText().toString();
                String password = passwordText.getText().toString();
                login(email, password);
            }
        });
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailText.getText().toString();
                String password = passwordText.getText().toString();
                signUp(email, password);
            }
        });

        buttonSignOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOff();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuth = null;
        emailText = null;
        passwordText = null;
        buttonLogin = null;
        buttonSignUp = null;
        buttonSignOff = null;
    }

    private void login(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    showToast("Now logged as: " + mAuth.getCurrentUser().getEmail());
                } else {
                    // If sign in fails, display a message to the user.
                    showToast("Login failed.");
                }
            }
        });
    }

    private void signUp(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    showToast("Signup success for user: " + mAuth.getCurrentUser().getEmail());
                } else {
                    // If sign in fails, display a message to the user.
                    showToast("Signup failed.");
                }
            }
        });
    }

    private void logOff(){
        if (mAuth != null){
            mAuth.signOut();
            Log.i("mainActivity", "Log Off" + mAuth.getCurrentUser());
            showToast("Usuário desconectado.");
        }
    }

    private void showToast(String text){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }
}
