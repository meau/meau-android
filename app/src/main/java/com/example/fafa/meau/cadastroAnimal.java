package com.example.fafa.meau;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.Iterator;
import java.util.Vector;

public class cadastroAnimal extends AppCompatActivity {

    private Fragment actionFragment;
    private Button buttonAdocao, buttonApadrinhar, buttonAjudar;

    private Context contexto; //necessario pra gaveta
    Drawer result; //necessario pra gaveta
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_animal);
        if(savedInstanceState != null) return;
        buttonAdocao = (Button) findViewById(R.id.buttonAdocao);
        buttonApadrinhar = (Button) findViewById(R.id.buttonApadrinhar);
        buttonAjudar = (Button) findViewById(R.id.buttonAjudar);

        //INICIO DO CODIGO DA GAVETA
        new DrawerBuilder().withActivity(this).build();

        Gaveta gaveta = new Gaveta();

        contexto = this;


        //create the drawer and remember the `Drawer` result object
        result = new DrawerBuilder()
                .withActivity(this)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {


                        Intent intent = gaveta.getIntentById(contexto, drawerItem.getIdentifier());
                        if (intent != null) {
                            startActivity(intent);
                            result.closeDrawer();
                            return true;
                        }


                        return false;
                    }
                })
                .build();

        //percorre vetor de items e povoa o menu
        Vector itemsGaveta = gaveta.getLista();
        Iterator it = itemsGaveta.iterator();
        while (it.hasNext()) {
            result.addItem((IDrawerItem) it.next());
        }

        //FIM DO CODIGO DA GAVETA
    }

    @Override
    protected void onStart() {
        super.onStart();
        buttonAdocao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionFragment = new CadastrarAnimalAdocao();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.activityCadastroAnimalFragment, actionFragment).commit();
            }
        });
        buttonApadrinhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionFragment = new CadastrarAnimalApadrinhar();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.activityCadastroAnimalFragment, actionFragment).commit();
            }
        });
        buttonAjudar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionFragment = new CadastrarAnimalAjudar();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.activityCadastroAnimalFragment, actionFragment).commit();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        actionFragment = null;
        buttonAdocao = null;
        buttonApadrinhar = null;
        buttonAjudar = null;
    }

    private void showToast(String text){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }
}
